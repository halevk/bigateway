import { IDashboardRepository } from "../../domain/repositories/dashboard";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { GetDashboardList, GetDashboardListResponse } from "../../messages/query/getDashboardList";
import { IsUserMemberOf } from "../../../core/extensions/role";
import { Role } from "../../../core/abstractions/access";
import { Dashboard, Right } from "../../domain/models/dashboard";
import { BasicDashboardInfo } from "../../messages/common/basicDashboardInfo";

export function GetDashboardListHandlerFactory(
    repo: IDashboardRepository,
    contextFactory: HttpContextFactory) {
    return async (req: GetDashboardList): Promise<GetDashboardListResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        let data: Dashboard[];
        if (!IsUserMemberOf(ctx.roles, [Role.Admin, Role.Master])) {
            if (IsUserMemberOf(ctx.roles, [Role.Designer])) {
                data = await repo.getByDesigner(ctx.customerid, ctx.userId);
            } else
                data = await repo.getByGroup(ctx.customerid, ctx.groups, Right.View);
        } else
            data = await repo.getByCustomer(ctx.customerid);
        if (data.length == 0) return { dashboards: [] };
        const dbs = data.map(p => {
            const di: BasicDashboardInfo = {
                id: p._id,
                active: p.active,
                createdBy: p.createdBy,
                createdOn: p.createdOn,
                updatedBy: p.updatedBy,
                updatedOn: p.updatedOn,
                title: p.title
            };
            return di;
        });
        return { dashboards: dbs };
    }
}