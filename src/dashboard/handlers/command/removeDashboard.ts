import { RemoveDashboard, RemoveDashboardResponse } from "../../messages/command/removeDashboard";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { IDashboardRepository } from "../../domain/repositories/dashboard";
import { IsUserMemberOf } from "../../../core/extensions/role";
import { Role } from "../../../core/abstractions/access";

export function RemoveDashboardHandlerFactory(
    repo: IDashboardRepository,
    contextFactory: HttpContextFactory    
) {
    return async (req: RemoveDashboard): Promise<RemoveDashboardResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const dashboard = await repo.findById(req.id);
        if (dashboard == null) return { success: false, msg: "dashboard not found" };
        if (IsUserMemberOf(ctx.roles, [Role.Designer]) && dashboard.designerid != ctx.userId)
            return { success: false, msg: "you can not remove this dashboard" };
        const isRemoved = await repo.remove(req.id);
        return { success: isRemoved, msg: "" };
    }
}