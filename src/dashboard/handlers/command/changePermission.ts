import { ChangePermission, ChangePermissionReponse } from "../../messages/command/changePermission";
import { IDashboardRepository } from "../../domain/repositories/dashboard";

export function ChangePermissionHandlerFactory(
    repo: IDashboardRepository) {
    return async (req: ChangePermission): Promise<ChangePermissionReponse> => {
        const db = await repo.findById(req.id);
        if (db == null) return { success: false, msg: "dashboard not found" };
        if (db.permissions.length == 0) {
            db.permissions.push({ groupid: req.groupid, rights: req.rights });
        } else {
            const perm = db.permissions.find(p => p.groupid == req.groupid);
            if (perm == undefined)
                db.permissions.push({ groupid: req.groupid, rights: req.rights });
            else perm.rights = req.rights;
        }
        const result = await repo.save(db);
        const msg = result ? "save successfully" : "permissions did not save";
        return { success: result, msg: msg };
    }
}