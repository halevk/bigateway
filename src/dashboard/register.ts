import { IMessageRouter } from "../core/abstractions/routing";
import { Container } from "inversify";
import { IDashboardRepository } from "./domain/repositories/dashboard";
import { DashboardRepository } from "./repositories/dashboard";
import { ChangeDashboardPermissionsRoute, SaveDashboardRoute, GetDashboardListRoute, GetDetailForDesignerRoute, RemoveDashboardRoute } from "./messages/routes";
import { SaveDashboardHandlerFactory } from "./handlers/command/saveDashboard";
import { IHttpContext } from "../core/abstractions/appcontext";
import { AppContextType } from "../core/abstractions/dependecyTypes";
import { SaveDashboardValidator } from "./messages/command/saveDashboard";
import { Role } from "../core/abstractions/access";
import { ChangePermissionHandlerFactory } from "./handlers/command/changePermission";
import { ChangePermissionValidator } from "./messages/command/changePermission";
import { GetDashboardListHandlerFactory } from "./handlers/query/getDashboardList";
import { GetDetailForDesignerHandlerFactory } from "./handlers/query/getDetailForDesginer";
import { GetDetailForDesignerValidator } from "./messages/query/getDetailForDesigner";
import { RemoveDashboardHandlerFactory } from "./handlers/command/removeDashboard";
import { RemoveDashboardValidator } from "./messages/command/removeDashboard";

export function RegisterDashboardTypes(container: Container): void {
    container.bind<IDashboardRepository>("IDashboardRepository").to(DashboardRepository).inSingletonScope();
}

function getDashboardRepository(container: Container): IDashboardRepository {
    return container.get<IDashboardRepository>("IDashboardRepository");
}

export function RegisterDashboardRouters(container: Container, router: IMessageRouter): void {
    router.registerCommandHandler(SaveDashboardRoute,
        SaveDashboardHandlerFactory(
            getDashboardRepository(container),
            () => container.get<IHttpContext>(AppContextType)),
        SaveDashboardValidator, [Role.Master, Role.Admin, Role.Designer]);

    router.registerCommandHandler(RemoveDashboardRoute,
        RemoveDashboardHandlerFactory(
            getDashboardRepository(container),
            () => container.get<IHttpContext>(AppContextType)),
        RemoveDashboardValidator, [Role.Master, Role.Admin, Role.Designer]);

    router.registerCommandHandler(ChangeDashboardPermissionsRoute,
        ChangePermissionHandlerFactory(getDashboardRepository(container)),
        ChangePermissionValidator, [Role.Master, Role.Admin, Role.Designer]);

    router.registerCommandHandler(GetDashboardListRoute,
        GetDashboardListHandlerFactory(
            getDashboardRepository(container),
            () => container.get<IHttpContext>(AppContextType)));

    router.registerCommandHandler(GetDetailForDesignerRoute,
        GetDetailForDesignerHandlerFactory(getDashboardRepository(container)),
        GetDetailForDesignerValidator, [Role.Master, Role.Admin, Role.Designer]);
}