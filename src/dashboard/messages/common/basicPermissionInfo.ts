export interface BasicPermissionInfo {
    groupid: string,
    rights: string[]
}