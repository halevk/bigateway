import { BasicPermissionInfo } from "./basicPermissionInfo";

export interface DetailedDashboardInfo {
    id: string,
    title: string,
    active: boolean,
    widgets: string[],
    permissions: BasicPermissionInfo[],
    createdBy:string,
    createdOn:string,
    updatedOn?:string,
    updatedBy?:string,
    layout: any
}