import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class ChangePermission {
    id: string = "";
    groupid: string = "";
    rights: string[] = [];
}

export interface ChangePermissionReponse {
    success: boolean,
    msg: string
}

export function ChangePermissionValidator(request: ChangePermission): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");
    if (IsNullOrEmptyOrWhitespace(request.groupid)) err.push("groupid is required");
    if (request.rights.length == 0) err.push("user rights are required");
    return { errors: err, success: err.length == 0 };
}