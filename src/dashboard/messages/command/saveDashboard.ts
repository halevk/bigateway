import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class SaveDashboard {
    id: string = "";
    title: string = "";
    active: boolean = false;
    widgets: string[] = [];
    layout: any = {};
}

export interface SaveDashboardResponse {
    success: boolean,
    msg: string,
    id?: string
}

export function SaveDashboardValidator(request: SaveDashboard): ValidationResult {
    const err: string[] = [];
    //if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");
    if (IsNullOrEmptyOrWhitespace(request.title)) err.push("title is required");
    //if (request.widgets.length == 0) err.push("widgets is required");
    if (request.layout == undefined || request.layout == null || request.layout == "") err.push("layout is required");
    const success = err.length == 0;
    return { errors: err, success: success };
}