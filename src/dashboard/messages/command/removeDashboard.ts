import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class RemoveDashboard {
    id: string = "";   
}

export interface RemoveDashboardResponse {
    success: boolean,
    msg: string    
}

export function RemoveDashboardValidator(request: RemoveDashboard): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");    
    const success = err.length == 0;
    return { errors: err, success: success };
}