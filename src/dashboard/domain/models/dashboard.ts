export enum Right {
    View = "view",
    Share = "share",
    Export = "export"
}
export interface Permission {
    groupid: string,
    rights: string[]
}
export interface Dashboard {
    _id: string,
    title: string,
    active: boolean,
    widgets: string[],
    permissions: Permission[],
    customerid:string,
    designerid:string,
    createdBy:string,
    createdOn:string,
    updatedOn?:string,
    updatedBy?:string,
    layout: any   
}