import { Dashboard, Right } from "../models/dashboard";

export interface IDashboardRepository {   
    findById(id: string): Promise<Dashboard | null>;
    save(data: Dashboard): Promise<boolean>;
    remove(id:string):Promise<boolean>;
    getByDesigner(customerid:string,designerid:string):Promise<Dashboard[]>;
    getByCustomer(customerid:string):Promise<Dashboard[]>;
    getByGroup(customerid:string,groups:string[],right:Right):Promise<Dashboard[]>;
}