export interface Field{
    name:string,
    alias:string,
    type:string,
    hidden:boolean
}
export interface Dataset {
    _id:string,
    name:string,
    alias:string,
    type:string, // hive,druid
    default:boolean,
    enable:boolean,
    fields:Field[]       
}