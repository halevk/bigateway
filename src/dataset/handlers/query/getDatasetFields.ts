import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { IDatasetRepository } from "../../domain/repositories/dataset";
import { GetDatasetFieldsResponse, GetDatasetFields } from "../../messages/query/getDatasetFields";
import { FieldWithHiddenInfo } from "../../messages/common/fieldInfo";

export function GetDatasetFieldsHandlerFactory(
    repo: IDatasetRepository,
    contextFactory: HttpContextFactory
) {
    return async (req: GetDatasetFields): Promise<GetDatasetFieldsResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        if (ctx.customerInfo) {
            const cds = ctx.customerInfo.datasets.find(p => p.id == req.id);
            if (cds) {
                const dataset = await repo.findById(req.id);
                if (dataset && dataset.enable) {
                    const fields = dataset.fields.map(o => {
                        const itm: FieldWithHiddenInfo = {
                            name: o.name,
                            alias: o.alias,
                            hidden: o.hidden,
                            type: o.type
                        };
                        return itm;
                    });
                    return { fields: fields };
                }
            }
        }
        return { fields: [] };
    }
}
