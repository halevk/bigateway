import { GetFieldsDefinition, GetFieldsDefinitionResponse } from "../../messages/query/getFieldsDefinition";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { SimpleFieldInfo } from "../../messages/common/fieldInfo";
import { IDataRequester, RequestType } from "../../../core/abstractions/dataRequester";
import { IDatasetRepository } from "../../domain/repositories/dataset";

export function GetFieldsDefnitionHandlerFactory(
    requester:IDataRequester,
    repo: IDatasetRepository    
) {
    return async (req: GetFieldsDefinition): Promise<GetFieldsDefinitionResponse> => {
        const dataset = await repo.findById(req.id);
        if (!dataset) return { fields: [] };
        const query = `SELECT COLUMN_NAME,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '${dataset.name}'`;
        const result = await requester.Query({ query: query }, RequestType.Sql);
        if (result.status == 200 && result.data) {
            const typeDict: any = {
                "BIGINT": "long",
                "VARCHAR": "string",
                "DOUBLE": "double",
                "FLOAT": "float",
                "TIMESTAMP": "timestamp"
            };
            const items = result.data as any[];
            const fields = items.map(p => {
                const itm: SimpleFieldInfo = { name: p.COLUMN_NAME, type: typeDict[p.DATA_TYPE] };
                return itm;
            });
            return { fields: fields };
        }
        return { fields: [] };
    }
}