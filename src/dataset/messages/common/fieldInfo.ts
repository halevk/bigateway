export interface FieldInfo {
    name:string,
    alias:string,
    type:string
}

export interface FieldWithHiddenInfo {
    name:string,
    alias:string,
    type:string,
    hidden:boolean
}

export interface SimpleFieldInfo {
    name:string,
    type:string
}
