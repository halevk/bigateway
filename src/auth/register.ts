import { Container } from "inversify/dts/container/container";
import { IMessageRouter, IMessageBroker } from "../core/abstractions/routing";
import { IUserRepository } from "./domain/repositories/user";
import { ITokenProvider } from "../core/abstractions/token";
import { SignInValidator } from "./messages/command/signin";
import { UserRepository } from "./repository/userRepository";
import {
    SingInRoute,
    SignOutRoute,
    ChangePasswordRoute,
    ChangeUserGroupsRoute,
    ChangeUserStatusRoute,
    SaveGroupRoute,
    PasswordRecoveryRoute,
    RemoveGroupRoute,
    RemoveUserRoute,
    GetUsersInGroupRoute,
    GetUsersOfCustomerRoute,
    GetGroupsRoute,
    SaveUserRoute,
    SaveUserAsMasterRoute
} from "./messages/routes";
import { ICache } from "../core/abstractions/caching";
import { CacheType, TokenProviderType, AppContextType, MessageBrokerType, AccessFilterType } from "../core/abstractions/dependecyTypes";
import { IGroupRepository } from "./domain/repositories/group";
import { GroupRepository } from "./repository/groupRepository";
import { IHttpContext } from "../core/abstractions/appcontext";
import { ChangePasswordHandlerFactory } from "./handlers/command/changePassword";
import { ChangePasswordValidator } from "./messages/command/changePassword";
import { ChangeUserGroupsHandlerFactory } from "./handlers/command/changeUserGroups";
import { ChangeUserGroupsValidator } from "./messages/command/changeUserGroups";
import { ChangeUserStatusHandlerFactory } from "./handlers/command/changeUserStatus";
import { ChangeUserStatusValidator } from "./messages/command/changeUserStatus";
import { SaveGroupHandlerFactory } from "./handlers/command/savegroup";
import { SaveGroupValidator } from "./messages/command/savegroup";
import { SaveUserHandlerFactory } from "./handlers/command/saveuser";
import { SaveUserValidator } from "./messages/command/saveuser";
import { PasswordRecoveryHandlerFactory } from "./handlers/command/passwordRecovery";
import { PasswordRecoveryValidator } from "./messages/command/passwordRecovery";
import { RemoveGroupHandlerFactory } from "./handlers/command/removeGroup";
import { RemoveGroupValidator } from "./messages/command/removeGroup";
import { RemoveUserHandlerFactory } from "./handlers/command/removeUser";
import { GetUsersInGroupValidator } from "./messages/query/getUsersInGroup";
import { GetUsersInGroupHandlerFactory } from "./handlers/query/getUsersInGroup";
import { GetUsersOfCustomerHandlerFactory } from "./handlers/query/getUsersOfCustomer";
import { SignInHandlerFactory } from "./handlers/command/signin";
import { SignOutHandlerFactory } from "./handlers/command/signout";
import { IAccessFilter } from "../core/abstractions/access";
import { interfaces } from "inversify";
import { Role } from "../core/abstractions/access";
import { AccessFilter } from "./filters/accessFilter";
import { GetGroupsHandlerFactory } from "./handlers/query/getGroups";
import { GetGroupsValidator } from "./messages/query/getGroups";
import { SaveUserAsMasterHandlerFactory } from "./handlers/command/saveuserasmaster";
import { SaveUserAsMasterValidator } from "./messages/command/saveuserasmaster";

function userRepositoryFactory(container: Container): IUserRepository {
    return container.get<IUserRepository>("IUserRepository");
}

function userGroupRepositoryFactory(container: Container): IGroupRepository {
    return container.get<IGroupRepository>("IGroupRepository");
}


export function RegisterAuthRouters(container: Container, router: IMessageRouter): void {
    router.registerCommandHandler(ChangePasswordRoute,
        ChangePasswordHandlerFactory(
            userRepositoryFactory(container),
            () => container.get<IHttpContext>(AppContextType)),
        ChangePasswordValidator);

    router.registerCommandHandler(ChangeUserGroupsRoute,
        ChangeUserGroupsHandlerFactory(userRepositoryFactory(container)),
        ChangeUserGroupsValidator,
        [Role.Admin, Role.Master]);

    router.registerCommandHandler(ChangeUserStatusRoute,
        ChangeUserStatusHandlerFactory(userRepositoryFactory(container)),
        ChangeUserStatusValidator,
        [Role.Admin, Role.Master]);

    router.registerCommandHandler(SaveGroupRoute,
        SaveGroupHandlerFactory(
            userGroupRepositoryFactory(container),
            () => container.get<IHttpContext>(AppContextType)),
        SaveGroupValidator,
        [Role.Admin, Role.Master]);

    router.registerCommandHandler(SaveUserRoute,
        SaveUserHandlerFactory(
            userRepositoryFactory(container),
            () => container.get<IHttpContext>(AppContextType)),
        SaveUserValidator,
        [Role.Admin, Role.Master]);

    router.registerCommandHandler(SaveUserAsMasterRoute,
        SaveUserAsMasterHandlerFactory(
            userRepositoryFactory(container)),
        SaveUserAsMasterValidator,
        [Role.Master]);

    router.registerCommandHandler(PasswordRecoveryRoute,
        PasswordRecoveryHandlerFactory(
            userRepositoryFactory(container),
            container.get<IMessageBroker>(MessageBrokerType)),
        PasswordRecoveryValidator);

    router.registerCommandHandler(RemoveGroupRoute,
        RemoveGroupHandlerFactory(
            userGroupRepositoryFactory(container),
            userRepositoryFactory(container),
            () => container.get<IHttpContext>(AppContextType)),
        RemoveGroupValidator, [Role.Admin, Role.Master]);

    router.registerCommandHandler(RemoveUserRoute,
        RemoveUserHandlerFactory(userRepositoryFactory(container)),
        RemoveGroupValidator, [Role.Admin, Role.Master]);

    router.registerCommandHandler(SingInRoute,
        SignInHandlerFactory(
            userRepositoryFactory(container),
            container.get<ITokenProvider>(TokenProviderType),
            container.get<ICache>(CacheType)),
        SignInValidator);

    router.registerCommandHandler(SignOutRoute,
        SignOutHandlerFactory(
            userRepositoryFactory(container),
            () => container.get<IHttpContext>(AppContextType),
            container.get<ICache>(CacheType)));

    router.registerCommandHandler(GetUsersInGroupRoute,
        GetUsersInGroupHandlerFactory(userRepositoryFactory(container)),
        GetUsersInGroupValidator, [Role.Admin, Role.Master]);

    router.registerCommandHandler(GetUsersOfCustomerRoute,
        GetUsersOfCustomerHandlerFactory(userRepositoryFactory(container), () => container.get<IHttpContext>(AppContextType)),
        undefined, [Role.Admin, Role.Master]);

    router.registerCommandHandler(GetGroupsRoute,
        GetGroupsHandlerFactory(userGroupRepositoryFactory(container)),
        GetGroupsValidator, [Role.Admin, Role.Master]);

}

export function RegisterAuthTypes(container: Container): void {
    container.bind<IUserRepository>("IUserRepository").to(UserRepository).inSingletonScope();
    container.bind<IGroupRepository>("IGroupRepository").to(GroupRepository).inSingletonScope();
    container.bind<IAccessFilter>(AccessFilterType).toDynamicValue((ctx: interfaces.Context) => {
        return new AccessFilter(() => container.get<IHttpContext>(AppContextType))
    }).inSingletonScope();
}