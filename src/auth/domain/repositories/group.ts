import { Group } from "../models/group";

export interface IGroupRepository {
    findByGroupId(id:string):Promise<Group | null>;    
    listByCustomerId(customerId:string):Promise<Group[]>;
    groupExists(name:string,customerId:string):Promise<boolean>;
    save(group:Group):Promise<boolean>;
    remove(id:string):Promise<boolean>;
}