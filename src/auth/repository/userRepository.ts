import { injectable, inject } from "inversify";
import { IUserRepository } from "../domain/repositories/user";
import { User } from "../domain/models/user";
import { IRepository } from "../../core/abstractions/repository";
import { RepositoryType } from "../../core/abstractions/dependecyTypes";

@injectable()
export class UserRepository implements IUserRepository {

    private repository: IRepository
    collectionName: string = "users";

    constructor(@inject(RepositoryType) repo: IRepository) {
        this.repository = repo;
    }

    findByEmail(email: string): Promise<User | null> {
        return this.repository.findOneByQuery<User>(this.collectionName, { "email": email });
    }

    findByUserId(userid: string): Promise<User | null> {
        return this.repository.findOneByQuery<User>(this.collectionName, { "_id": userid });
    }

    listByCustomerId(customerId: string): Promise<User[]> {
        return this.repository.find<User>(this.collectionName, { "customerid": customerId }, { "name": 1 }, 0, 1000);
    }

    save(user: User): Promise<boolean> {
        return this.repository.save(this.collectionName, user._id, user);
    }

    remove(userid: string): Promise<boolean> {
        return this.repository.removeSingle(this.collectionName, userid);
    }

    async userExistsInGroup(customerid: string, groupid: string): Promise<boolean> {
        const user = await this.repository.findOneByQuery<User>(this.collectionName,
            {
                "customerid": customerid,
                "groups": groupid
            });
        return user != null;
    }
    usersInGroup(groupid: string): Promise<User[]> {
        return this.repository.find<User>(this.collectionName, { "groups": groupid }, { "name": 1 }, 0, 1000);
    }
}