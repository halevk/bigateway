import { IGroupRepository } from "../domain/repositories/group";
import { injectable, inject } from "inversify";
import { Group } from "../domain/models/group";
import { IRepository } from "../../core/abstractions/repository";
import { RepositoryType } from "../../core/abstractions/dependecyTypes";

@injectable()
export class GroupRepository implements IGroupRepository {

    private repository: IRepository
    private collectionName: string = "groups";
    constructor(@inject(RepositoryType) repo: IRepository) {
        this.repository = repo;
    }

    findByGroupId(id: string): Promise<Group | null> {
        return this.repository.findOneById<Group>(this.collectionName, id);
    }
    listByCustomerId(customerId: string): Promise<Group[]> {
        return this.repository.find<Group>(this.collectionName, { "customerid": customerId }, null, 0, 1000);
    }
    save(group: Group): Promise<boolean> {
        return this.repository.save<Group>(this.collectionName, group._id, group);
    }

    async groupExists(name: string, customerId: string): Promise<boolean> {
        const group = await this.repository.findOneByQuery<Group>(this.collectionName, { "customerid": customerId, "name": name });       
        return group != null;
    }

    remove(id: string): Promise<boolean> {
        return this.repository.removeSingle(this.collectionName, id);
    }
}