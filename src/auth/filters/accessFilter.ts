import { HttpContextFactory, IAppContext, AppContextTypes } from "../../core/abstractions/appcontext";
import { injectable } from "inversify";
import { IAccessFilter, Role } from "../../core/abstractions/access";
import { IsUserMemberOf } from "../../core/extensions/role";

@injectable()
export class AccessFilter implements IAccessFilter {
    private factory: HttpContextFactory;    
    constructor(factory: HttpContextFactory ) {
        this.factory = factory;        
    }

    async canAccess(roles?: Role[] | undefined): Promise<boolean> {
        if (roles == undefined) return true;
        const ctx = this.factory().Get<IAppContext>(AppContextTypes.Context);
        return IsUserMemberOf(ctx.roles,roles)       
    }
}
