import { GetUsersOfCustomer, GetUsersOfCustomerResponse } from "../../messages/query/getUsersOfCustomer";
import { IUserRepository } from "../../domain/repositories/user";
import {  IAppContext, AppContextTypes, HttpContextFactory } from "../../../core/abstractions/appcontext";
import { BasicUserInfo } from "../../messages/common/basicUser";

export function GetUsersOfCustomerHandlerFactory (userRepo: IUserRepository, contextFactory: HttpContextFactory) {
    return async (req: GetUsersOfCustomer): Promise<GetUsersOfCustomerResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const users = await userRepo.listByCustomerId(ctx.customerid);
        if (users == null || users.length == 0) return { users: [] };
        const mappedUsers = users.map(p => {
            const item: BasicUserInfo = { id: p._id, name: p.name, surname: p.surname };
            return item;
        });
        return { users: mappedUsers };
    }
}