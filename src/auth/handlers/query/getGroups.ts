import { IGroupRepository } from "../../domain/repositories/group";
import { GetGroups, GetGroupsResponse } from "../../messages/query/getGroups";
import { BasicGroup } from "../../messages/common/basicGroup";

export function GetGroupsHandlerFactory(repo: IGroupRepository) {
    return async (req: GetGroups): Promise<GetGroupsResponse> => {
        const grps = await repo.listByCustomerId(req.customerid);
        if (grps.length == 0) return { groups: [] };
        const mappedGroups = grps.map(p => {
            const bg: BasicGroup = { id: p._id, name: p.name };
            return bg;
        });
        return { groups: mappedGroups };
    }
}