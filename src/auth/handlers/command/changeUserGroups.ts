import { ChangeUserGroups, ChangeUserGroupsResponse } from "../../messages/command/changeUserGroups";
import { IUserRepository } from "../../domain/repositories/user";

export function ChangeUserGroupsHandlerFactory (userRepo: IUserRepository) {
    return async (req: ChangeUserGroups): Promise<ChangeUserGroupsResponse> => {
        const user = await userRepo.findByUserId(req.id);
        if (!user) return { success: false, msg: ["user not found"] }
        if (req.groups.length > 0) user.groups = req.groups;       
        const result = await userRepo.save(user);
        if (!result) return { success: false, msg: ["user groups did not changed"] };
        return { success: true, msg: ["groups successfully changed"] };
    }
}