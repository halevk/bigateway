import { IUserRepository } from "../../domain/repositories/user";
import { RemoveUser, RemoveUserResponse } from "../../messages/command/removeUser";

export function RemoveUserHandlerFactory(userRepo: IUserRepository) {
    return async (req: RemoveUser): Promise<RemoveUserResponse> => {
        const user = await userRepo.findByUserId(req.id);
        if (user == null) return { success: false, msg: ["user not found"] };        
        const result = await userRepo.remove(req.id);
        if (!result) return { success: false, msg: ["user could not removed"] }
        return { success: true, msg: ["user removed successfully"] }
    }
}