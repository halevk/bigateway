import { IAppContext, AppContextTypes, IHttpContext, HttpContextFactory } from "../../../core/abstractions/appcontext";
import { SaveGroup, SaveGroupResponse } from "../../messages/command/savegroup";
import { IGroupRepository } from "../../domain/repositories/group";
import { Group } from "../../domain/models/group";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
const uuidv4 = require("uuid/v4");

export function SaveGroupHandlerFactory(
    groupRepo: IGroupRepository,
    factory: HttpContextFactory) {
    return async (req: SaveGroup): Promise<SaveGroupResponse> => {
        const ctx = factory().Get<IAppContext>(AppContextTypes.Context);        
        const exists = await groupRepo.groupExists(req.name, ctx.customerid);
        if (exists) return { success: false, msg: ["group has already exists"] }
        const id = IsNullOrEmptyOrWhitespace(req.id) ? uuidv4() : req.id;
        const userGroup: Group = {
            _id: id,
            name: req.name,
            customerid: ctx.customerid
        };
        const result = await groupRepo.save(userGroup);
        if (!result) return { success: false, msg: ["group could not created"] }
        return { success: true, msg: ["group created successfully"] }
    }
}