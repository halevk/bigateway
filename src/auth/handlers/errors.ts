export enum AuthErrorMessages {
    InvalidCredentials="invalid_credentials",
    AccountDisabled="account_disabled"
}