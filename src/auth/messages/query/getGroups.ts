import { BasicGroup } from "../common/basicGroup";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { ValidationResult } from "../../../core/abstractions/routing";

export class GetGroups {
    customerid:string="";
}

export interface GetGroupsResponse {
    groups:BasicGroup[]
}

export function GetGroupsValidator(request:GetGroups):ValidationResult{
    const err:string[]=[];
    if(IsNullOrEmptyOrWhitespace(request.customerid)) err.push("customer is required");
    const success = err.length==0;
    return {errors:err,success:success};
}