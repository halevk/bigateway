import { BasicUserInfo } from "../common/basicUser";

export class GetUsersOfCustomer{}

export interface GetUsersOfCustomerResponse{    
    users:BasicUserInfo[]
}

