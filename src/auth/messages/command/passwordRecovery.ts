import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class PasswordRecovery{
    email:string=""; 
}

export interface PasswordRecoveryResponse{
    success:boolean,
    msg:string  
}


export function PasswordRecoveryValidator(request:PasswordRecovery):ValidationResult{
    const err:string[]=[];
    if(IsNullOrEmptyOrWhitespace(request.email)) err.push("email is required"); 
    const success = err.length==0;
    return {errors:err,success:success};

}