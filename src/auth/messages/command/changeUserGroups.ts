import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class ChangeUserGroups {
    id: string = "";
    groups: string[] = [];
    adminGroups: string[] = [];
}

export interface ChangeUserGroupsResponse {
    success: boolean,
    msg: string[] | undefined,
}


export function ChangeUserGroupsValidator(req: ChangeUserGroups): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(req.id)) err.push("id is required");
    if(req.groups.length==0 && req.adminGroups.length==0) err.push("please select user groups");
    const success = err.length == 0;
    return { errors: err, success: success };

}