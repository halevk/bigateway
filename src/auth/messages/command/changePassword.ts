import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class ChangePassword{
    oldpwd:string=""; 
    newpwd:string="";
    retypednewpwd:string="";
}

export interface ChangePasswordResponse{
    success:boolean,
    msg:string  
}


export function ChangePasswordValidator(request:ChangePassword):ValidationResult{
    const err:string[]=[];
    if(IsNullOrEmptyOrWhitespace(request.oldpwd)) err.push("please enter old password"); 
    if(IsNullOrEmptyOrWhitespace(request.newpwd)) err.push("please enter new password"); 
    if(IsNullOrEmptyOrWhitespace(request.retypednewpwd)) err.push("please enter retype new password"); 
    if(request.newpwd!=request.retypednewpwd) err.push("new password does not macth retyped new password");
    const success = err.length==0;
    return {errors:err,success:success};

}