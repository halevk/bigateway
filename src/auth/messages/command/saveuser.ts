import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { DatasetInfo } from "../../../core/abstractions/appcontext";

export class SaveUser {
    id: string = "";
    name: string = "";
    surname: string = "";
    email: string = "";
    pwd: string = "";
    groups: string[] = [];
    roles: string[] = [];
    datasets: DatasetInfo[] = [];   
    isEnabled: boolean = false; 
}

export interface SaveUserResponse {
    success: boolean,
    msg: string[] | undefined,
    id?: string
}


export function SaveUserValidator(request: SaveUser): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.name)) err.push("name is required");
    if (IsNullOrEmptyOrWhitespace(request.surname)) err.push("surname is required");
    if (IsNullOrEmptyOrWhitespace(request.email)) err.push("email is required");
    if (IsNullOrEmptyOrWhitespace(request.pwd) && IsNullOrEmptyOrWhitespace(request.pwd)) err.push("password is required");    
    //if (request.groups.length == 0) err.push("user group is required");
    const success = err.length == 0;
    return { errors: err, success: success };

}