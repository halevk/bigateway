export function IsNullOrEmptyOrWhitespace(val:string):boolean{   
    if(!val) return true;
    if(val=="" || val==" ") return true;
    return false;
}