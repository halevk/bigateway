import { Widget } from "../models/widget";
import { QueryInfo } from "../models/queryInfo";

export interface IWidgetRepository {
    save(data: Widget): Promise<boolean>;
    remove(id:string):Promise<boolean>;
    findById(id: string): Promise<Widget | null>;
    findByCustomerId(customerid: string): Promise<Widget[]>;
    findByIds(ids: string[]): Promise<Widget[]>;
    getQueries(ids:string[]):Promise<QueryInfo[]>;
}