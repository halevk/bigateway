export interface DataSource {
    id: string,
    query: any[]
}
export interface Widget {
    _id: string,
    title: string,
    createdBy:string,
    createdOn:string,
    updatedBy?:string,
    updatedOn?:string,
    creatorid:string,
    type: string,
    subtype: string,
    data: DataSource[],
    sourcesToFilter: any,
    options: any,
}
