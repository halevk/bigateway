import { IWidgetRepository } from "../../domain/repositories/widget";
import { WidgetDetail, WidgetDetailResponse } from "../../messages/query/widgetDetail";
import { DataSourceInfo } from "../../messages/common/dataSourceInfo";

export function WidgetDetailHandlerFactory(repo: IWidgetRepository) {
    return async (req: WidgetDetail): Promise<WidgetDetailResponse | null> => {
        const widget = await repo.findById(req.id);
        if (widget == null) return null;
        return {
            id: widget._id,
            createdBy: widget.createdBy,
            createdOn: widget.createdOn,
            data: widget.data.map(p => {
                const itm: DataSourceInfo = { id: p.id, query: p.query };
                return itm;
            }),
            options: widget.options,
            sourcesToFilter: widget.sourcesToFilter,
            subtype: widget.subtype,
            title: widget.title,
            type: widget.type
        };
    }
}