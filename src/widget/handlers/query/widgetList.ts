import { IWidgetRepository } from "../../domain/repositories/widget";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { WidgetList, WidgetListResponse } from "../../messages/query/widgetList";
import { BasicWidgetInfo } from "../../messages/common/basicWidgetInfo";

export function WidgetListHandlerFactory(
    repo: IWidgetRepository,
    contextFactory: HttpContextFactory) {
    return async (req: WidgetList): Promise<WidgetListResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const widgets = await repo.findByCustomerId(ctx.customerid);
        const winfos = widgets.map(p => {
            const itm: BasicWidgetInfo = {
                id: p._id,
                createdBy: p.createdBy,
                createdOn: p.createdOn,
                subtype: p.subtype,
                title: p.title,
                tpe: p.type
            };
            return itm;
        });
        return { widgets: winfos };
    }
}