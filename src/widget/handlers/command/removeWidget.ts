import { RemoveWidget, RemoveWidgetResponse } from "../../messages/command/removeWidget";
import { IWidgetRepository } from "../../domain/repositories/widget";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { IsUserMemberOf } from "../../../core/extensions/role";
import { Role } from "../../../core/abstractions/access";

export function RemoveWidgetHandlerFactory(
    repo: IWidgetRepository,
    contextFactory: HttpContextFactory) {
    return async (req: RemoveWidget): Promise<RemoveWidgetResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const widget = await repo.findById(req.id);
        if (widget == null) return { success: false, msg: "widget not found" };
        if (IsUserMemberOf(ctx.roles, [Role.Designer]) && ctx.userId != widget.creatorid)
            return { success: false, msg: "you can not remove this widget" };
        const result = await repo.remove(req.id);
        return { success: result, msg: "" };
    }
}