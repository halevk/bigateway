import { IWidgetRepository } from "../../domain/repositories/widget";
import { SaveWidget, SaveWidgetResponse } from "../../messages/command/saveWidget";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { Widget, DataSource } from "../../domain/models/widget";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { GetUtcNowAsString } from "../../../core/extensions/date";
const uuidv4 = require("uuid/v4");

export function SaveWidgetHandlerFactory(
    repo: IWidgetRepository, contextFactory: HttpContextFactory) {
    return async (req: SaveWidget): Promise<SaveWidgetResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const isIdMissing = IsNullOrEmptyOrWhitespace(req.id)
        const id = isIdMissing ? uuidv4() : req.id;
        const user = `${ctx.name} ${ctx.surname}`;
        let widget: Widget | null = null;
        let datasources: DataSource[] = [];
        if (req.data.length > 0)
            datasources = req.data.map(p => {
                const itm: DataSource = { id: p.id, query: p.query };
                return itm;
            });
        if (!isIdMissing) {
            widget = await repo.findById(id);
            if (widget == null) return { success: false, msg: "widget not found" };
            widget.updatedOn = GetUtcNowAsString();
            widget.updatedBy = user;
            widget.options = req.options;
            widget.sourcesToFilter = req.sourcesToFilter;
            widget.title = req.title;
            widget.subtype = req.subtype;
            widget.type = req.type;
            widget.data = datasources;
        }
        else {
            widget = {
                _id: id,
                data: datasources,
                createdBy: user,
                createdOn: GetUtcNowAsString(),
                creatorid: ctx.userId,
                options: req.options,
                sourcesToFilter: req.sourcesToFilter,
                title: req.title,
                subtype: req.subtype,
                type: req.type
            };
        }
        const result = await repo.save(widget);
        if (!result) return { success: false, msg: "widget could not saved" };
        return { success: true, msg: "", id: id };
    }
}