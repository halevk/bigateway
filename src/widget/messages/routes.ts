export const SaveWidgetRoute = "/widget/save";
export const WidgetListRoute = "/widget/list";
export const WidgetDetailRoute = "/widget/detail";
export const WidgetQueriesRoute="/widget/query";
export const RemoveWidgetRoute="/widget/remove";