import { WidgetQuery } from "../common/widgetQuery";
import { ValidationResult } from "../../../core/abstractions/routing";
import { isDate } from "util";

export class WidgetQueries {
    ids: string[] = [];
}

export interface WidgetQueriesResponse {
    queries: WidgetQuery[];
}

export function WidgetQueriesValidator(request: WidgetQueries): ValidationResult {
    const err: string[] = [];
    if (isDate.length==0) err.push("ids are required");      
    const success = err.length == 0;
    return { errors: err, success: success };
}