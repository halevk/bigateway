import { BasicWidgetInfo } from "../common/basicWidgetInfo";

export class WidgetList{}

export interface WidgetListResponse {
    widgets:BasicWidgetInfo[];
}