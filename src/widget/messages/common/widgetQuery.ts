import { DataSourceInfo } from "./dataSourceInfo";

export interface WidgetQuery {
    id:string,
   datasource:DataSourceInfo[]
}