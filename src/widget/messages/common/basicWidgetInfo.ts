export interface BasicWidgetInfo {
    id:string,
    title:string,
    tpe:string,
    subtype:string,
    createdBy:string,
    createdOn:string
}