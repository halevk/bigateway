import { DataSourceInfo } from "../common/dataSourceInfo";
import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export class SaveWidget {
    id: string = "";
    title: string = "";
    type: string = "";
    subtype: string = "";
    data: DataSourceInfo[] = [];
    sourcesToFilter: any = null;
    options: any = null;
}

export interface SaveWidgetResponse {
    success: boolean,
    msg: string,
    id?: string
}

export function SaveWidgetValidator(request: SaveWidget): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.title)) err.push("title is required");  
    if (IsNullOrEmptyOrWhitespace(request.type)) err.push("type is required"); 
    if (IsNullOrEmptyOrWhitespace(request.subtype)) err.push("subtype is required");  
    //if(request.data.length==0) err.push("datasource is required");
    const success = err.length == 0;
    return { errors: err, success: success };
}