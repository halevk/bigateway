import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import { AppContextTypes, IAppContext, IHttpContext } from "../../core/abstractions/appcontext";
import { Role } from "../../core/abstractions/access";
import { Container } from "inversify";
import { AppContextType } from "../../core/abstractions/dependecyTypes";
import { IUserRepository } from "../../auth/domain/repositories/user";
import { SingInRoute, SignOutRoute, PasswordRecoveryRoute } from "../../auth/messages/routes";
import { IsUserMemberOf } from "../../core/extensions/role";
import { ICustomerRepository } from "../../customer/domain/abstractions";
import { IDatasetRepository } from "../../dataset/domain/repositories/dataset";
const uuidv4 = require("uuid/v4");

export default (tokenKey: string, container: Container) => {
    return async (req: Request, resp: Response, next: NextFunction) => {
        const httpContext = container.get<IHttpContext>(AppContextType);
        const routes: string[] = [SingInRoute, SignOutRoute, PasswordRecoveryRoute];
        const contains = routes.find(p => p == req.url);
        if (contains != undefined) {
            next();
            return;
        }

        const authHeader = req.headers.authorization || "";
        if (authHeader == null || authHeader == undefined || authHeader == "") {
            resp.status(401).send({ status: false, error: "auth token is required" });
            return;
        }
        const [type, token] = authHeader.split(" ");
        if (type.toLowerCase() != "bearer") {
            resp.status(401).send({ status: false, error: "auth type must be bearer" });
            return;
        }
        try {
            let decoded: any = {};
            try {
                decoded = jwt.verify(token, tokenKey) as any;
            } catch (err) {
                resp.status(401).send({ status: false, error: "token is invalid" });
                return;
            }
            const userRepo = container.get<IUserRepository>("IUserRepository");
            const user = await userRepo.findByUserId(decoded.id);
            if (user == null) {
                resp.status(401).send({ status: false, error: "there is no such user" });
                return;
            }
            var ip = ((req.headers['x-forwarded-for'] || '') as string).split(',').pop() ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress;            
            const context: IAppContext = {
                ip: ip,
                name: decoded.name,
                userId: decoded.id,
                requestId: uuidv4(),
                customerid: user.customerid,
                roles: user.roles,
                surname: user.surname,
                groups: user.groups,
                route: req.url
            };
            if (!IsUserMemberOf(user.roles, [Role.Master])) {
                const customerRepo = container.get<ICustomerRepository>("ICustomerRepository");
                const datasetRepo = container.get<IDatasetRepository>("IDatasetRepository");
                const customer = await customerRepo.findById(user.customerid);
                const dss = customer != null ? await datasetRepo.findByIds(customer.datasets.map(p => p.id)) : [];
                if (customer != null) {
                    customer.datasets.forEach((itm) => {
                        const ds = dss.find(p => p._id == itm.id);
                        if (ds != undefined) itm.name = ds.name;
                    });
                    context.customerInfo = {
                        erpid: customer.erpid,
                        datasets: customer.datasets
                    };
                }
                if (user.datasets) {
                    user.datasets.forEach((itm) => {
                        const ds = dss.find(p => p._id == itm.id);
                        if (ds != undefined) itm.name = ds.name;
                    });
                }
                context.datasets = user.datasets;
            }
            httpContext.Set(AppContextTypes.Context, context);
        } catch (err) {
            resp.status(500).send({ status: false, error: "an error occured" });
            return;
        }
        next();
    }
}