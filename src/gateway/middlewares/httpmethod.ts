import { Request, Response, NextFunction } from "express";

export default () => {
    return (req: Request, resp: Response, next: NextFunction) => {
        if (req.method.toLowerCase() != "post") {
            resp.status(400);            
            return;
        }
        next();        
    }
}