export interface MetricLog {
    _id:string,
    ip?: string,
    route: string,
    userid: string,
    user: string,
    requestid: string
    totalexec: number,
    date: string
}

export interface RequestLog {
    _id:string,
    ip?: string,
    route: string,
    userid: string,
    user: string,
    requestid: string,
    date: string,
    error?: string,
    req: string,
    resp: string
}