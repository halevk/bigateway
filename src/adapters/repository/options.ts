import { MongoClient } from "mongodb";

export interface IRepositoryOptions {
    dbName: string,
    client: MongoClient
}