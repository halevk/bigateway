import { Container } from "inversify/dts/container/container";
import { ILogger } from "../../core/abstractions/logging";
import { MongoLogger } from "./mongoLogger";
import { LoggerType } from "../../core/abstractions/dependecyTypes";
import { ILoggerOptions } from "./options";

export function RegisterLoggerTypes (options:ILoggerOptions,container:Container):void{
    container.bind<ILoggerOptions>("ILoggerOptions").toConstantValue(options);
    container.bind<ILogger>(LoggerType).to(MongoLogger).inSingletonScope();
}