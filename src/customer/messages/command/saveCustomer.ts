import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { DatasetInfo } from "../../../core/abstractions/appcontext";

export class SaveCustomer {
    id: string = "";
    name: string = "";
    erpid: string = "";   
    datasets: DatasetInfo[] = [];
}

export interface SaveCustomerReponse {
    success: boolean,
    msg: string
}

export function SaveCustomerValidator(request: SaveCustomer): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.id)) err.push("id is required");
    if (IsNullOrEmptyOrWhitespace(request.name)) err.push("name is required");        
    const success = err.length == 0;
    return { errors: err, success: success };

}