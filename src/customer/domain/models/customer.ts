import { DatasetInfo } from "../../../core/abstractions/appcontext";

export interface Customer {
    _id: string,
    name: string,
    erpid:string,          
    createdOn:string,
    createdBy:string,
    datasets:DatasetInfo[]
}

