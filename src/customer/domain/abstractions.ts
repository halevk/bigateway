import { Customer } from "./models/customer";

export interface ICustomerRepository {
    save(cust: Customer): Promise<boolean>;
    findById(id: string): Promise<Customer | null>;
    listCustomers():Promise<Customer[]>;
}