export interface IEmailOptions {
    subject:string,
    from:string,
    to:string,
    cc:string,
    bcc:string,
    body:string,
    isbodyhtml:boolean    
}

export interface SendingReport{
    success:boolean,
    msg:string
}

export interface ISmtpNotifier{
    Send(options:IEmailOptions):Promise<SendingReport>;
}