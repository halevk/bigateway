import { ISmtpNotifier, IEmailOptions, SendingReport } from "../core/abstraction";
import mailer from "nodemailer";
import Mail from "nodemailer/lib/mailer";
import { injectable, inject } from "inversify";
import { IEmailNotifyOptions } from "../options";

@injectable()
export class SmtpNotifier implements ISmtpNotifier {
    private transporter: Mail;
    constructor(@inject("IEmailNotifyOptions") options: IEmailNotifyOptions) {
        const config: any = {
            service: options.service,
            auth: options.auth
        };
        this.transporter = mailer.createTransport(config)
    }

    async Send(options: IEmailOptions): Promise<SendingReport> {
        let msg: Mail.Options = {
            from: options.from,
            to: options.to,
            subject: options.subject,
            bcc: options.bcc
        };
        if (options.isbodyhtml)
            msg.html = options.body;
        else msg.text = options.body;
        const sentInfo = await this.transporter.sendMail(msg);
        const success = sentInfo.error == null;
        return { success: success, msg: sentInfo.error };
    }
}