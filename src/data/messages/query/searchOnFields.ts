import { ValidationResult } from "../../../core/abstractions/routing";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";

export interface QueryInfo {
    type: string,
    value: string
}
export class SearchOnFields {
    datasource: string = "";
    dimension: string = "";
    query?: QueryInfo = undefined;
    intervals: string[] = [];
}

export interface SearchOnFieldsResponse {    
    items: string[]
}

export function SearchOnFieldsValidator(request: SearchOnFields): ValidationResult {
    const err: string[] = [];
    if (IsNullOrEmptyOrWhitespace(request.datasource)) err.push("datasource is required");
    if (IsNullOrEmptyOrWhitespace(request.dimension)) err.push("dimension is required");
    if (request.intervals.length == 0) err.push("interval is required");
    const success = err.length == 0;
    return { errors: err, success: success };
}