import { SearchOnFields, SearchOnFieldsResponse } from "../../messages/query/searchOnFields";
import { HttpContextFactory, IAppContext, AppContextTypes } from "../../../core/abstractions/appcontext";
import { IsUserMemberOf } from "../../../core/extensions/role";
import { Role } from "../../../core/abstractions/access";
import { IsNullOrEmptyOrWhitespace } from "../../../core/extensions/stringextensions";
import { GetDataset, GetPermissions } from "../../domain/queryExtensions";
import { IDataRequester, RequestType } from "../../../core/abstractions/dataRequester";

export function SearchOnFieldsHandlerFactory(
    requester: IDataRequester,
    contextFactory: HttpContextFactory) {

    function getSearchQuery(req: SearchOnFields): any {
        let requestModel: any = {
            "queryType": "search",
            "dataSource": req.datasource,
            //"granularity": "day",
            "searchDimensions": [req.dimension],
            "sort": {
                "type": "lexicographic"
            },
            "intervals": req.intervals
        };
        if (req.query &&
            !IsNullOrEmptyOrWhitespace(req.query.type) &&
            !IsNullOrEmptyOrWhitespace(req.query.value)) {
            requestModel.query = {
                "type": req.query.type,
                "value": req.query.value
            };
        }
        return requestModel;
    }

    async function getFieldValues(query: any): Promise<string[]> {
        try {
            const resp = await requester.Query(query,RequestType.Native);
            if (resp.status != 200) return [];
            const data: any[] = resp.data;
            if (data.length == 0) return [];
            const items: any[] = data[0].result;
            return items.map(p => p.value);
        } catch (e) {
            console.log(e);
        }
        return [];
    }

    return async (req: SearchOnFields): Promise<SearchOnFieldsResponse> => {
        const ctx = contextFactory().Get<IAppContext>(AppContextTypes.Context);
        const query = getSearchQuery(req);
        let vals: string[] = [];       
        if (IsUserMemberOf(ctx.roles, [Role.Master]))
            vals = await getFieldValues(query);
        else {
            const ds = GetDataset(req.datasource, ctx.customerInfo);
            if (ds == undefined) return { items: [] };
            const permissions = GetPermissions(ds, ctx);
            const fieldInfo = permissions.find(p => p.name == req.dimension);
            if (fieldInfo) {
                const val = req.query ? req.query.value : "";
                vals = (val != "") ? fieldInfo.values.filter(p => p.indexOf(val) > -1) : fieldInfo.values;                
            }
            else vals = await getFieldValues(query);
        }
        return { items: vals };
    }
}