import { Container } from "inversify";
import { IMessageRouter } from "../core/abstractions/routing";
import { SearchSourceHandlerFactory } from "./handlers/query/searchSource";
import { SearchSourceRoute, SearchOnFieldsRoute, SearchBySqlRoute } from "./messages/routes";
import { IHttpContext } from "../core/abstractions/appcontext";
import { AppContextType } from "../core/abstractions/dependecyTypes";
import { Role } from "../core/abstractions/access";
import { SearchSourceValidator } from "./messages/query/searchSource";
import { SearchOnFieldsValidator } from "./messages/query/searchOnFields";
import { SearchOnFieldsHandlerFactory } from "./handlers/query/searchOnFields";
import { IDataRequester } from "../core/abstractions/dataRequester";
import { SearchBySqlHandlerFactory } from "./handlers/query/searchBySql";
import { SearchBySqlValidator } from "./messages/query/searchBySql";

function getDataOptions(container: Container): IDataRequester {
    return container.get<IDataRequester>("IDataRequester");
}

export function RegisterDataRouters(container: Container, router: IMessageRouter): void {
    router.registerCommandHandler(SearchSourceRoute,
        SearchSourceHandlerFactory(
            getDataOptions(container),
            () => container.get<IHttpContext>(AppContextType)),
        SearchSourceValidator, [Role.Admin, Role.Master, Role.Designer]);

    router.registerCommandHandler(SearchBySqlRoute,
        SearchBySqlHandlerFactory(
            getDataOptions(container),
            () => container.get<IHttpContext>(AppContextType)),
        SearchBySqlValidator, [Role.Admin, Role.Master, Role.Designer]);

    router.registerCommandHandler(SearchOnFieldsRoute,
        SearchOnFieldsHandlerFactory(
            getDataOptions(container),
            () => container.get<IHttpContext>(AppContextType)),
        SearchOnFieldsValidator, [Role.Admin, Role.Master, Role.Designer]);
}


