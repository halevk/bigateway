import { describe } from "mocha";
import { expect } from "chai";
import { CheckQuery, BuildSqlQuery } from "../handlers/common/sqlQueryExtension";
import { SearchBySql } from "../messages/query/searchBySql";
import { IAppContext } from "../../core/abstractions/appcontext";

describe("Druid Sql query Test", function () {
    it("should_check_empty_query", function () {
        const sql: SearchBySql = { queries: [] };
        const ctx: IAppContext = {
            userId: "",
            name: "",
            customerid: "1",
            groups: [],
            roles: [],
            requestId: "",
            route: "",
            surname: ""
        };
        const items = CheckQuery(sql, ctx);
        expect(items.length).to.be.equals(0);
    });

    it("should_check_from_and_select", function () {
        const sql: SearchBySql = {
            queries: [{
                from: "",
                select: "",
                groupby: "",
                having: "",
                limit: -1,
                orderby: "",
                where: []
            }]
        };
        const ctx: IAppContext = {
            userId: "",
            name: "",
            customerid: "1",
            groups: [],
            roles: [],
            requestId: "",
            route: "",
            surname: ""
        };
        const items = CheckQuery(sql, ctx);
        expect(items.length).to.be.equals(0);
    });

    it("should_check_from_on_customer_dataset", function () {
        const sql: SearchBySql = {
            queries: [{
                from: "abcd",
                select: "name,surname",
                groupby: "",
                having: "",
                limit: -1,
                orderby: "",
                where: []
            }]
        };
        const ctx: IAppContext = {
            userId: "",
            name: "",
            customerid: "1",
            groups: [],
            roles: [],
            requestId: "",
            route: "",
            surname: "",
            customerInfo: {
                erpid: "1",
                datasets: [
                    {
                        id: "",
                        name: "def",
                        permissions: []
                    }
                ]
            }
        };
        const items = CheckQuery(sql, ctx);
        expect(items.length).to.be.equals(0);
    });

    it("should_check_customer_dataset_restriction", function () {
        const sql: SearchBySql = {
            queries: [{
                from: "abcd",
                select: "name,surname",
                groupby: "",
                having: "",
                limit: -1,
                orderby: "",
                where: []
            }]
        };
        const ctx: IAppContext = {
            userId: "",
            name: "",
            customerid: "1",
            groups: [],
            roles: [],
            requestId: "",
            route: "",
            surname: "",
            customerInfo: {
                erpid: "1",
                datasets: [
                    {
                        id: "",
                        name: "abcd",
                        permissions: [
                            {
                                name: "accountroot",
                                values: ["XXXXXXXX", "YYYYYYYYYY", "ZZZZZZZZZZZ"],
                                hidden: false
                            }
                        ]
                    }
                ]
            }
        };
        const items = CheckQuery(sql, ctx);
        expect(items.length).to.be.equals(1);
        expect(items[0].where.length).to.be.equals(1);
    });

    it("should_check_user_dataset_restriction", function () {
        const sql: SearchBySql = {
            queries: [{
                from: "abcd",
                select: "name,surname",
                groupby: "",
                having: "",
                limit: -1,
                orderby: "",
                where: []
            }]
        };
        const ctx: IAppContext = {
            userId: "",
            name: "",
            customerid: "1",
            groups: [],
            roles: [],
            requestId: "",
            route: "",
            surname: "",
            datasets: [
                {
                    id: "",
                    name: "abcd",
                    permissions: [
                        {
                            name: "accountroot",
                            hidden: false,
                            values: ["XXXXXXXXX", "YYYYYYYYYYYY"]
                        }
                    ]
                }
            ],
            customerInfo: {
                erpid: "1",
                datasets: [
                    {
                        id: "",
                        name: "abcd",
                        permissions: [
                            {
                                name: "accountroot",
                                values: ["XXXXXXXX", "YYYYYYYYYY", "ZZZZZZZZZZZ"],
                                hidden: false
                            }
                        ]
                    }
                ]
            }
        };
        const items = CheckQuery(sql, ctx);
        expect(items.length).to.be.equals(1);
        expect(items[0].where[0].field).to.be.equals("accountroot");
    });

    it("should_build_empty_query", function () {
        const query = BuildSqlQuery([]);
        expect(query).equals("");
    });

    it("should_build_simple_query", function () {
        const query = BuildSqlQuery([
            {
                select: "name,surname",
                from: "abcd",
                groupby: "",
                having: "",
                limit: -1,
                orderby: "",
                where: []
            }]);
        expect(query).contains("SELECT");
        expect(query).contains("FROM");
    });
    it("should_build_query", function () {
        const query = BuildSqlQuery([
            {
                select: "accountroot,country,sum(searchcount)",
                from: "dr_hsl",
                groupby: "accountroot,country",
                having: "",
                limit: 10,
                orderby: "searchcount",
                where: [
                    {
                        cond: "in",
                        field: "country",
                        opr: "and",
                        value: ["TR", "GB"]
                    },
                    {
                        cond: "=",
                        field: "accountroot",
                        opr: "and",
                        value: "Test Account"
                    },
                    {
                        cond: "=",
                        field: "adultcount",
                        opr: "",
                        value: 35
                    }
                ]
            }]);
        console.log(query);
    });    
});